BOLD=$'\[\033[1m\]'
RED=$'\[\033[31m\]'
GREEN=$'\[\033[32m\]'
YELLOW=$'\[\033[33m\]'
BLUE=$'\[\033[34m\]'
MAGENTA=$'\[\033[35m\]'
BRIGHTYELLOW=$'\[\033[93m\]'
UNFORMAT=$'\[\033[m\]'
COLOR="${GREEN}"
SYMBOL="\$"
WORKINGDIR=

[ "$(whoami)" = "root" ] && COLOR="$RED" && SYMBOL=$'\043'
[ "$1" != "" ] && COLOR="$1"

export PS1="${UNFORMAT}\
\
${BOLD}\
${COLOR}\
$(id -un)\
\
${YELLOW}\
@\
\
${BLUE}\
$(uname -n)\
\
${YELLOW}\
:\
\
${MAGENTA}\
\\w\
\
${COLOR}\
${SYMBOL}\
\
${UNFORMAT}\
 \
"

unset BOLD
unset RED
unset GREEN
unset YELLOW
unset BLUE
unset MAGENTA
unset BRIGHTYELLOW
unset UNFORMAT
unset COLOR
unset SYMBOL
